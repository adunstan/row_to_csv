
#include "postgres.h"

#if PG_VERSION_NUM >= 90300
#include "access/htup_details.h"
#endif
#include "catalog/pg_type.h"
#include "executor/spi.h"
#include "lib/stringinfo.h"
#include "libpq/pqformat.h"
#include "mb/pg_wchar.h"
#include "parser/parse_coerce.h"
#include "parser/parse_type.h"
#include "utils/array.h"
#include "utils/builtins.h"
#include "utils/lsyscache.h"
#include "utils/typcache.h"

PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(row_to_csv);
extern Datum row_to_csv(PG_FUNCTION_ARGS);


typedef struct
{
    char delimiter;
    char *null_string;
    char quote_char;
    char escape_char;
	/* might need force quote here later */
} row_to_csv_options;

/*
 * Turn a scalar Datum into a CSV field, quoting as necessary.
 */
static inline void
datum_to_csvfield (Datum val, bool is_null, StringInfo result, 
				   row_to_csv_options *opts, Oid typoutputfunc)
{

	char *outputstr;
	int len;
	char search[] = "xx\r\n";

	if (is_null)
	{
		appendStringInfoString(result,opts->null_string);
		return;
	}

	outputstr = OidOutputFunctionCall(typoutputfunc, val);
	len = strlen(outputstr);

	search[0] = opts->delimiter;
	search[1] = opts->quote_char;


	/* 
	 * quoting rules. If it contains the quote char or the delimiter char or
	 * a CR or LF or it's the same as the null string it needs to be quoted.
	 * In quoted fields, the above plus the escape char are preceded by 
	 * the escape char.
	 */
	if (strcspn(outputstr,search) != len ||
		strcmp(outputstr,opts->null_string) == 0)
	{
		int i;

		/* opening quote */
		appendStringInfoChar(result,opts->quote_char);
		
		/* escape anything that needs it */
		for (i = 0; i < len; i++)
		{
			char c = outputstr[i];
			if (c == opts->quote_char || c == opts->escape_char)
			{
				appendStringInfoChar(result,opts->escape_char);
			}

			appendStringInfoChar(result,c);
		}
				
		/* closing quote */
		appendStringInfoChar(result,opts->quote_char);
	}
	else
	{
		/* nothing that needs quoting here */
		appendStringInfoString(result,outputstr);
	}

	pfree(outputstr);

}

/*
 * Turn a composite / record into a CSV line.
 */
static void
composite_to_csv(Datum composite, StringInfo result, row_to_csv_options *opts)
{
    HeapTupleHeader td;
    Oid         tupType;
    int32       tupTypmod;
    TupleDesc   tupdesc;
    HeapTupleData tmptup, *tuple;
	int         i;
	bool        needsep = false;

    td = DatumGetHeapTupleHeader(composite);

    /* Extract rowtype info and find a tupdesc */
    tupType = HeapTupleHeaderGetTypeId(td);
    tupTypmod = HeapTupleHeaderGetTypMod(td);
    tupdesc = lookup_rowtype_tupdesc(tupType, tupTypmod);

    /* Build a temporary HeapTuple control structure */
    tmptup.t_len = HeapTupleHeaderGetDatumLength(td);
    tmptup.t_data = td;
	tuple = &tmptup;

    for (i = 0; i < tupdesc->natts; i++)
    {
        Datum       val, origval;
        bool        isnull;
		Oid			typoutput;
		bool		typisvarlena;
#if PG_VERSION_NUM >= 110000
		Form_pg_attribute att = TupleDescAttr(tupdesc, i);
#else
		Form_pg_attribute att = tupdesc->attrs[i];
#endif
		if (att->attisdropped)
            continue;

		if (needsep)
			appendStringInfoChar(result,opts->delimiter);
		needsep = true;

        origval = heap_getattr(tuple, i + 1, tupdesc, &isnull);

		getTypeOutputInfo(att->atttypid,
						  &typoutput, &typisvarlena);

		/*
		 * If we have a toasted datum, forcibly detoast it here to avoid memory
		 * leakage inside the type's output routine.
		 */
		if (typisvarlena && ! isnull)
			val = PointerGetDatum(PG_DETOAST_DATUM(origval));
		else
			val = origval;

		datum_to_csvfield(val, isnull, result, opts, typoutput);

		/* Clean up detoasted copy, if any */
		if (val != origval)
			pfree(DatumGetPointer(val));
	}

	appendStringInfoString(result,"\n");
    ReleaseTupleDesc(tupdesc);
}

/*
 * SQL function row_to_csv(row [, optionkey, optionval ...])
 */
extern Datum
row_to_csv(PG_FUNCTION_ARGS)
{
	Datum    row = PG_GETARG_DATUM(0);
	ArrayType *options = PG_GETARG_ARRAYTYPE_P(1);

	StringInfo	result;
	row_to_csv_options *opt =  (row_to_csv_options *) fcinfo->flinfo->fn_extra;

	if (opt == NULL || true)
	{
		row_to_csv_options argopt;
		int		   *dim;
		int			ndim;
		int			nitems;
		int         i;
		Datum	   *elements;
		bool       *nulls;

		MemoryContext oldcontext = CurrentMemoryContext;


		MemoryContextSwitchTo(fcinfo->flinfo->fn_mcxt);

		opt = palloc(sizeof(row_to_csv_options));

		ndim = ARR_NDIM(options);
		dim = ARR_DIMS(options);
		nitems = ArrayGetNItems(ndim, dim);

		/*
		  elog(NOTICE,"items: %d",nitems);
		*/

		if (nitems %2 != 0)
		{
		  elog(ERROR,"options mut ch key,value pairs");
		}

		argopt.delimiter = '\0';
		argopt.null_string = NULL;
		argopt.quote_char = '\0';
		argopt.escape_char = '\0';

		deconstruct_array(options, TEXTOID, -1, false, 'i',
						  &elements, &nulls, &nitems);


		for (i = 0; i < nitems; i+= 2)
		{
			text *key,*val;
			char keystr[64], valstr[64];
			key = DatumGetTextP(elements[i]);
			val = DatumGetTextP(elements[i+1]);
			if ( key == NULL || val == NULL)
				elog(ERROR,"NULL option names or values not allowed");
			text_to_cstring_buffer(key,keystr,64);
			text_to_cstring_buffer(val,valstr,64);

			if (pg_strcasecmp(keystr,"delimiter") == 0)
			{
				if (strlen(valstr) != 1)
					elog(ERROR,"delimiter must be one byte");
				if (argopt.delimiter != '\0')
					elog(ERROR,"delimiter must only be specified once");
				argopt.delimiter = valstr[0];
			}
			else if (pg_strcasecmp(keystr,"null") == 0)
			{
				if (argopt.null_string != NULL)
					elog(ERROR,"null must only be specified once");
				argopt.null_string = pstrdup(valstr);;
			}
			else if (pg_strcasecmp(keystr,"quote") == 0)
			{
				if (strlen(valstr) != 1)
					elog(ERROR,"quote must be one byte");
				if (argopt.quote_char != '\0')
					elog(ERROR,"quote must only be specified once");
				argopt.quote_char = valstr[0];	
			}
			else if (pg_strcasecmp(keystr,"escape") == 0)
			{
				if (strlen(valstr) != 1)
					elog(ERROR,"escape must be one byte");
				if (argopt.escape_char != '\0')
					elog(ERROR,"escape must only be specified once");
				argopt.escape_char = valstr[0];			
			}
			else
			{
				elog(ERROR,"invalid option %s", keystr);
			}
		}

		if (argopt.delimiter != '\0')
			opt->delimiter = argopt.delimiter;
		else
			opt->delimiter = ',';

		if (argopt.null_string != NULL)
			opt->null_string = argopt.null_string;
		else
			opt->null_string = "";

		if (argopt.quote_char != '\0')
			opt->quote_char = argopt.quote_char;
		else
			opt->quote_char = '"';

		if (argopt.escape_char != '\0')
			opt->escape_char = argopt.escape_char;
		else
			opt->escape_char = opt->quote_char;

		if (opt->delimiter == opt->escape_char || opt->delimiter == opt->quote_char)
			elog(ERROR,"delimiter must not be same as quote or escape");

		MemoryContextSwitchTo(oldcontext);

		fcinfo->flinfo->fn_extra = opt;


		/*
		 elog(NOTICE,"delimiter: '%c', quote: '%c', escape: '%c', null: \"%s\"",
			 opt->delimiter, opt->quote_char, opt->escape_char, opt->null_string);
		*/
	}

	result = makeStringInfo();

	composite_to_csv(row, result, opt);

	PG_RETURN_TEXT_P(cstring_to_text(result->data));
};

