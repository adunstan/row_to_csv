/*
 * Author: Andrew Dunstan
 * Created at: April 07 2012
 *
 */ 


CREATE FUNCTION row_to_csv(record, variadic options text[] default '{}')
RETURNS text
AS 'MODULE_PATHNAME','row_to_csv'
LANGUAGE C STRICT IMMUTABLE;
